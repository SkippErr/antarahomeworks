import model.Kotik;

public class Application {
    public static void main(String[] args) {
        Kotik cat1 = new Kotik();
        Kotik cat2 = new Kotik("Коди", "Персиковый", "Мяу - Мяу - Мяу", 1, 3.5F, 5);

        System.out.println("Котика зовут " + cat1.getName());
        System.out.println("Он весит " + cat1.getWeight() + " кг");
        System.out.println();
        cat1.liveAnotherDay();
        System.out.println();

        if (cat1.getMeow().equals(cat2.getMeow())) {
            System.out.println("Котики мяукают одинаково!");
        }
        System.out.println("Котики мяукают по-разному!");

        System.out.println();
        System.out.println("За время выполнения программы на свет появились " + Kotik.catCounting() + " котик(а/ов)");
    }
}
