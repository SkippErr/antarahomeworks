package model;

public class Kotik {
    private static int countOfCats = 0;
    private String name;
    private String color;
    private String meow;
    private int age;
    private float weight;
    private int satiety;

    public Kotik() {
        setCat();
        countOfCats++;
    }

    public Kotik(String name, String color, String meow, int age, float weight, int satiety) {
        countOfCats++;
        this.name = name;
        this.color = color;
        this.meow = meow;
        this.age = age;
        this.weight = weight;
        this.satiety = satiety;
    }

    public static int catCounting() {
        return countOfCats;
    }

    private void setCat() {
        name = "Ти Джей Диллашоу";
        color = "Рыжий";
        meow = "Мур - мур -мур";
        age = 2;
        weight = 4;
        satiety = 0;
    }

    public String getName() {
        return name;
    }

    public String getColor() {
        return color;
    }

    public String getMeow() {
        return meow;
    }

    public int getAge() {
        return age;
    }

    public float getWeight() {
        return weight;
    }

    public int getSatiety() {
        return satiety;
    }

    public boolean isHungry() {
        return getSatiety() <= 0;
    }

    public boolean play() {
        if (isHungry()) {
            System.out.println("Как я могу играть, если я голоден? =(");
            return false;
        }
        System.out.println("Котик " + getName() + " играет с клубком ниток!");
        satiety--;
        return true;
    }

    public boolean sleep() {
        if (isHungry()) {
            System.out.println("Котик не может уснуть, пока его одолевает чувство голода!");
            return false;
        }
        System.out.println("Котику снятся сны, но он не помнит, что там...");
        return true;
    }

    public boolean chaseMouse() {
        if (isHungry()) {
            System.out.println(getColor() + " котик преследует мышь, потому что он очень голоден.");
            return true;
        }
        System.out.println(getColor() + " котику нет никакого дела до мышки, ведь он сыт и доволен собой))");
        return false;
    }

    public boolean walkOnTheStreet() {
        if (isHungry()) {
            System.out.println("Котик отказывается выходить на улицу и ждёт, пока его покормят");
            return false;
        }
        System.out.println("После сытного обеда котик отправляется на улицу к своим друзьям!");
        return true;
    }

    public boolean voice() {
        if (isHungry()) {
            System.out.println("Котик истошно мяукает в надежде, что его покормят!");
            return true;
        }
        System.out.println("Котик сыт и спокоен.");
        return false;
    }

    public boolean eat(int portion) {
        satiety += portion;
        return true;
    }

    public boolean eat(int portion, String food) {
        System.out.println("Котик " + getName() + " культурно кушает " + food);
        return eat(portion);
    }

    public boolean eat() {
        eat(2, "колбаску");
        return true;
    }


    public void liveAnotherDay() {
        for (int i = 0; i < 24; i++) {
            int actions = 5;
            int choise = (int) (Math.random() * actions + 1);
            switch (choise) {
                case 1:
                    if (!sleep()) {
                        eat();
                    }
                    break;
                case 2:
                    if (!play()) {
                        eat();
                    }
                    break;
                case 3:
                    if (!chaseMouse()) {
                        eat();
                    }
                    break;
                case 4:
                    if (!walkOnTheStreet()) {
                        eat();
                    }
                    break;
                case 5:
                    if (!voice()) {
                        eat();
                    }
                    break;
            }
        }
    }
}
