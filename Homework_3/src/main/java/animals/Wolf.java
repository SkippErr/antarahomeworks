package animals;

import actions.Run;
import actions.Voice;
import enclosure.Capacity;

public class Wolf extends Carnivorous implements Run, Voice {

    public Wolf(String name) {
        this.name = "Волк " + name;
        satietly = 0;
        enclosureSize = Capacity.MEDIUM;
    }

    public void run() {
        System.out.println(getName() + " расхаживает по вольеру");
        satietly--;
    }

    public String voice() {
        return "Воет на луну";
    }
}
