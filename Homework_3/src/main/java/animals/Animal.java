package animals;

import enclosure.Capacity;
import food.Food;

public abstract class Animal {

    protected String name;
    protected int satietly;
    protected Capacity enclosureSize;

    public String getName() {
        return name;
    }

    public void getSatietly() {
        if (satietly <= 0) {
            System.out.println(getName() + " истощен(а)");
            return;
        }
        System.out.println("Сытость " + getName() + ": " + satietly);
    }

    public abstract boolean eat(Food food);

    public Capacity getEnclosureSize() {
        return enclosureSize;
    }

    public boolean equals(Object obj_animal) {
        Animal animal = (Animal) obj_animal;
        if (this == animal) return true;
        if (this.getClass() != animal.getClass()) return false;
        return (this.getName().equals(animal.getName())) & this.getEnclosureSize().equals(animal.getEnclosureSize());
    }

    public int hashCode() {
        return name.hashCode() + enclosureSize.hashCode();
    }

}
