package animals;

import actions.Run;
import actions.Voice;
import enclosure.Capacity;

public class Zebra extends Herbivore implements Run, Voice {

    public Zebra(String name) {
        this.name = "Рыба " + name;
        satietly = 0;
        enclosureSize = Capacity.HUGE;
    }

    public void run() {
        System.out.println(getName() + " скачет галопом");
        satietly--;
    }

    public String voice() {
        return " ржёт";
    }
}
