package animals;

import actions.Run;
import actions.Voice;
import enclosure.Capacity;

public class Bear extends Carnivorous implements Run, Voice {

    public Bear(String name) {
        this.name = "Медведь " + name;
        satietly = 0;
        enclosureSize = Capacity.HUGE;
    }

    public void run() {
        System.out.println(getName() + " ходит вперевалочку");
        satietly--;
    }

    public String voice() {
        return "рычит";
    }
}
