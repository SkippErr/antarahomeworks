package animals;

import actions.Swim;
import enclosure.Capacity;

public class Fish extends Herbivore implements Swim {

    public Fish(String name) {
        this.name = "Рыба " + name;
        satietly = 0;
        enclosureSize = Capacity.MEDIUM;
    }

    public void swim() {
        System.out.println(getName() + " плавает в пруду");
        satietly--;
    }
}
