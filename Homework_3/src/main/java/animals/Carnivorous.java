package animals;

import food.Food;
import food.Meat;
import food.WrongFoodExeption;

public abstract class Carnivorous extends Animal {

    public boolean eat(Food food) {
        if (food instanceof Meat) {
            System.out.println(getName() + " кушает " + food.getName());
            satietly += food.getSaturation();
            return true;
        } else {
            System.out.println("Это не подходящая еда для животного " + getName());
            try {
                throw new WrongFoodExeption();
            } catch (WrongFoodExeption e) {
                e.printStackTrace();
            }
            return false;
        }
    }
}
