package food;

public abstract class Food {
    protected String name;
    protected int saturation;

    public abstract String getName();

    public abstract int getSaturation();
}
