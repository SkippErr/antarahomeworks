package food;

public class Meat extends Food {

    public Meat(String name, int saturation) {
        this.name = name;
        this.saturation = saturation;
    }

    public String getName() {
        return name;
    }

    public int getSaturation() {
        return saturation;
    }
}
