package food;

public class WrongFoodExeption extends Exception {

    public WrongFoodExeption() {
        super("Не подходящая еда для данного животного!");
    }
}
