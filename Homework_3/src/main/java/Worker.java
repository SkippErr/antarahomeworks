import actions.Fly;
import actions.Run;
import actions.Voice;
import animals.Animal;
import food.Food;

public class Worker {

    private String name;

    public Worker(String name) {
        setName(name);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void feed(Animal animal, Food food) {
        animal.eat(food);
    }

    public void getVoice(Voice animal) {
        System.out.println(animal.voice());
    }

    public void getFly(Fly animal) {
        animal.fly();
    }

    public void getRun(Run animal) {
        animal.run();
    }

}
