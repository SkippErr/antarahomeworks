import actions.Swim;
import animals.*;
import enclosure.Capacity;
import enclosure.Enclosure;
import food.Grass;
import food.Meat;

public class Zoo {

    public static void main(String[] args) {

        Worker worker = new Worker("Виталий");
        System.out.println("Работник " + worker.getName() + " вышел на смену");

        Carnivorous bear = new Bear("Анатолий");
        Carnivorous wolf = new Wolf("Макс");
        Lion lion = new Lion("Лёва");

        Herbivore zebra = new Zebra("Зибра");
        Duck duck = new Duck("Люся");
        Fish fish = new Fish("Николай");

        Meat schnitzel = new Meat("Шницель", 3);
        Meat chickenLeg = new Meat("Куриная ножка", 2);
        Grass grass = new Grass("Трава", 2);
        Grass hay = new Grass("Сено", 2);

        worker.getVoice(lion);
        //worker.getVoice(fish); // проверка по заданию, пункт 6.
        worker.getFly(duck);
        worker.getRun((Wolf) wolf); // для разнообразия (Приведение типов)

        worker.feed(wolf, chickenLeg);
        worker.feed(bear, hay);
        worker.feed(duck, schnitzel);
        worker.feed(zebra, grass);

        Swim[] pond = {fish, duck};
        System.out.println("Создан пруд с двумя животными");
        for (Swim animals : pond) {
            animals.swim();
        }
        System.out.println();

        wolf.getSatietly();
        bear.getSatietly();
        duck.getSatietly();
        zebra.getSatietly();
        System.out.println();

        System.out.println("Создан ОГРОМНЫЙ вольер для хищных животных");
        Enclosure<Carnivorous> enclosureHugeC = new Enclosure<>(Capacity.HUGE);
        enclosureHugeC.addAnimal(lion);
        enclosureHugeC.addAnimal(wolf);
        enclosureHugeC.addAnimal(bear);
        //enclosureHugeC.addAnimal(fish); Привёдет к исключению, т.к. тип животного не соответствует типу вольера.
        enclosureHugeC.showAnimals();
        System.out.println();

        System.out.println("Создан СРЕДНИЙ вольер для хищных животных");
        Enclosure<Carnivorous> enclosureMediumC = new Enclosure<>(Capacity.MEDIUM);
        enclosureMediumC.addAnimal(lion);
        enclosureMediumC.addAnimal(wolf);
        //enclosureMediumC.addAnimal(duck); Привёдет к исключению, т.к. тип животного не соответствует типу вольера.
        enclosureMediumC.showAnimals();
        System.out.println();

        System.out.println("Создан СРЕДНИЙ вольер для травоядных");
        Enclosure<Herbivore> enclosureMediumH = new Enclosure<>(Capacity.MEDIUM);
        enclosureMediumH.addAnimal(duck);
        enclosureMediumH.addAnimal(fish);
        //enclosureMediumH.addAnimal(wolf); Привёдет к исключению, т.к. тип животного не соответствует типу вольера.
        enclosureMediumH.showAnimals();
        System.out.println();

        System.out.println("Создан МАЛЕНЬКИЙ вольер для травоядных");
        Enclosure<Herbivore> enclosureSmallH = new Enclosure<>(Capacity.SMALL);
        enclosureSmallH.addAnimal(duck);
        //enclosureSmallH.addAnimal(zebra); Привёдет к исключению, т.к. тип животного не соответствует типу вольера.
        enclosureSmallH.showAnimals();
        System.out.println();

        System.out.println("Получаем по имени объект из большого вольера для хищников и присваеваем его переменной");
        Animal wolf1 = enclosureSmallH.getAnimal("Крыса");
        System.out.println("Выводим на экран имя извлеченного из вольера животного");
        System.out.println(wolf1);
        System.out.println("Удаляем животное из вольера по ссылке на объект");
        enclosureHugeC.removeAnimal(wolf);
        System.out.println("Выводим на экран оставшихся животных в вольере");
        enclosureHugeC.showAnimals();
        System.out.println();

        Wolf wolf2 = new Wolf("Антон");
        System.out.println("hashcode lion1 = " + wolf.hashCode());
        System.out.println("hashcode lion3 = " + wolf2.hashCode());
        System.out.println("Проверка равенства lion1 and lion3: " + wolf.equals(wolf2));
    }
}
