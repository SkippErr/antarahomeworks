package enclosure;

import animals.Animal;

import java.util.HashSet;

public class Enclosure<T extends Animal> {

    private HashSet<T> animalSet;
    Capacity capacity;

    public Enclosure(Capacity capacity) {
        this.capacity = capacity;
        animalSet = new HashSet<>(0);
    }

    public boolean isSuitable(T e) {
        switch (e.getEnclosureSize()) {
            case HUGE: {
                if (capacity == Capacity.HUGE) return true;
                break;
            }
            case BIG: {
                if (capacity == Capacity.BIG || capacity == Capacity.HUGE) return true;
                break;
            }
            case MEDIUM: {
                if (capacity == Capacity.MEDIUM || capacity == Capacity.BIG || capacity == Capacity.HUGE)
                    return true;
                break;
            }
            case SMALL: {
                return true;
            }
        }
        return false;
    }

    public void addAnimal(T e) {
        if (isSuitable(e)) {
            animalSet.add(e);
        }
        System.out.println("Размер вольера не подходит для " + e.getName());
    }

    public void removeAnimal(T e) {
        animalSet.remove(e);
    }

    public T getAnimal(String name) {
        for (T obj : animalSet) {
            if (obj.getName().equals(name)) {
                return obj;
            }
        }
        return null;
    }

    public void showAnimals() {
        System.out.println("В вольере сейчас находятся: ");
        for (T animal : animalSet) {
            System.out.println(animal.getName());
        }
    }
}
