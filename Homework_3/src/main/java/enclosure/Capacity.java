package enclosure;

public enum Capacity {
    SMALL(1),
    MEDIUM(2),
    BIG(3),
    HUGE(4);

    private int capacity;

    Capacity(int capacity) {
        this.capacity = capacity;
    }
}
