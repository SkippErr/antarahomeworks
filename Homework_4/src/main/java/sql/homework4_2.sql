/*1*/
SELECT p.model, p.speed, p.hd
FROM PC p
WHERE PRICE < 500;

/*2*/
SELECT DISTINCT p.maker
FROM Product p
WHERE type = 'Printer';

/*3*/
SELECT l.model, l.ram, l.screen
FROM Laptop l
WHERE price > 1000;

/*4*/
SELECT *
FROM Printer
WHERE color = 'y';

/*5*/
SELECT p.model, p.speed, p.hd
FROM PC p
WHERE cd IN ('12x', '24x') AND price < 600;

/*6*/
SELECT DISTINCT pt.maker AS Maker, lp.speed
FROM Product pt
INNER JOIN Laptop lp
ON lp.model = pt.model
WHERE lp.hd >= 10;

/*7*/
SELECT pc.model, price
FROM PC pc INNER JOIN
     Product pt ON pc.model = pt.model
WHERE maker = 'B'
UNION
SELECT lp.model, price
FROM Laptop lp INNER JOIN
     Product pt ON lp.model = pt.model
WHERE maker = 'B'
UNION
SELECT pr.model, price
FROM Printer pr INNER JOIN
     Product pt ON pr.model = pt.model
WHERE maker = 'B';

/*8*/
SELECT DISTINCT maker
FROM Product
WHERE type = 'PC' AND maker NOT IN (
    SELECT maker
    FROM Product
    WHERE type = 'Laptop'
    GROUP BY maker
);

/*9*/
SELECT DISTINCT maker
FROM Product INNER JOIN PC
ON Product.model = PC.model
WHERE speed >= 450;

/*10*/
SELECT model, price
FROM Printer
WHERE price = (
    SELECT MAX(price)
    FROM Printer);

/*11*/
SELECT AVG(speed)
FROM PC;

/*12*/
SELECT AVG(speed)
FROM Laptop
WHERE price > 1000;

/*13*/
SELECT AVG(speed)
FROM PC INNER JOIN Product
ON PC.model = Product.model
WHERE maker = 'A';


/*14*/
SELECT s.class, s.name, c.country
FROM Ships s INNER JOIN Classes c
ON s.class = c.class
WHERE numGuns >= 10;

/*15*/
SELECT PC.hd
FROM PC
GROUP BY hd
HAVING count(model) > 1;

/*16*/
SELECT pc1.model, pc2.model, pc1.speed, pc1.ram
FROM PC pc1, PC pc2
WHERE pc1.speed = pc2.speed AND pc1.ram = pc2.ram AND pc1.model > pc2.model
ORDER BY pc1.model, pc2.model;

/*17*/
SELECT pt.type, lp.model, lp.speed
FROM Laptop lp INNER JOIN Product pt ON lp.model = pt.model
WHERE lp.speed < ALL (SELECT speed FROM PC);

/*18*/
SELECT maker, price
FROM Printer INNER JOIN Product
ON Printer.model = Product.model AND Printer.color = 'y'
WHERE price = (SELECT MIN(price) FROM Printer WHERE color = 'y');

/*19*/
SELECT maker, AVG(lp.screen)
FROM Product pt INNER JOIN Laptop lp
ON pt.model = lp.model
GROUP BY maker;

/*20*/
SELECT maker, COUNT(model) AS Count_Model
FROM Product
WHERE type = 'PC'
GROUP BY maker
HAVING COUNT(model) > 2;

/*21*/
SELECT maker, MAX(PC.price)
FROM Product INNER JOIN PC
ON Product.model = PC.model
GROUP BY maker;
