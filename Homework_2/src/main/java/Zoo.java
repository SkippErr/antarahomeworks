import actions.Swim;
import animals.Bear;
import animals.Wolf;
import animals.Lion;
import animals.Duck;
import animals.Zebra;
import animals.Fish;
import animals.Carnivorous;
import animals.Herbivore;
import food.Grass;
import food.Meat;

public class Zoo {

    public static void main(String[] args) {

        Worker worker = new Worker("Виталий");
        System.out.println("Работник " + worker.getName() + " вышел на смену");

        Carnivorous bear = new Bear("Анатолий");
        Carnivorous wolf = new Wolf("Макс");
        Lion lion = new Lion("Лёва");

        Herbivore zebra = new Zebra("Зибра");
        Duck duck = new Duck("Люся");
        Fish fish = new Fish("Николай");

        Meat schnitzel = new Meat("Шницель", 3);
        Meat chickenLeg = new Meat("Куриная ножка", 2);
        Grass grass = new Grass("Трава", 2);
        Grass hay = new Grass("Сено", 2);

        worker.getVoice(lion);
        //worker.getVoice(fish); // проверка по заданию, пункт 6.
        worker.getFly(duck);
        worker.getRun((Wolf) wolf); // для разнообразия (Приведение типов)

        worker.feed(wolf, chickenLeg);
        worker.feed(bear, hay);
        worker.feed(duck, schnitzel);
        worker.feed(zebra, grass);

        Swim[] pond = {fish, duck};
        for (Swim animals : pond) {
            animals.swim();
        }

        wolf.getSatietly();
        bear.getSatietly();
        duck.getSatietly();
        zebra.getSatietly();
    }
}
