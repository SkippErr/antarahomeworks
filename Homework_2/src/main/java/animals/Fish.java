package animals;

import actions.Swim;

public class Fish extends Herbivore implements Swim {

    public Fish(String name) {
        this.name = "Рыба " + name;
        satietly = 0;
    }

    public void swim() {
        System.out.println(getName() + " плавает в пруду");
        satietly--;
    }
}
