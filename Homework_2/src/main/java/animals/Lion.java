package animals;

import actions.Voice;
import actions.Run;

public class Lion extends Carnivorous implements Run, Voice {

    public Lion(String name) {
        this.name = "Лев " + name;
        satietly = 0;
    }

    public void run() {
        System.out.println(getName() + " рыщет по вольеру в поисках пищи");
        satietly--;
    }

    public String voice() {
        return "Рычит";
    }
}
