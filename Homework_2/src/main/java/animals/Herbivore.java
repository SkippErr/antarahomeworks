package animals;

import food.Food;
import food.Grass;

public abstract class Herbivore extends Animal {

    public boolean eat(Food food) {
        if (food instanceof Grass) {
            System.out.println(getName() + " ест " + food.getName());
            satietly += food.getSaturation();
            return true;
        } else {
            System.out.println("Эта еда не подходит животному " + getName());
            return false;
        }
    }
}
