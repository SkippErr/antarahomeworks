package animals;

import actions.Fly;
import actions.Run;
import actions.Swim;
import actions.Voice;

public class Duck extends Herbivore implements Fly, Swim, Voice, Run {

    public Duck(String name) {
        this.name = "Утка " + name;
        satietly = 0;
    }

    public void fly() {
        System.out.println(getName() + " летает");
        satietly--;
    }

    public void swim() {
        System.out.println(getName() + " плавает и ныряет в пруде");
        satietly--;
    }

    public String voice() {
        return " крякает";
    }

    public void run() {
        System.out.println(getName() + " бегает по вольеру");
        satietly--;
    }
}
