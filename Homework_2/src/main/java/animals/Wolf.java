package animals;

import actions.Voice;
import actions.Run;

public class Wolf extends Carnivorous implements Run, Voice {

    public Wolf(String name) {
        this.name = "Волк " + name;
        satietly = 0;
    }

    public void run() {
        System.out.println(getName() + " расхаживает по вольеру");
        satietly--;
    }

    public String voice() {
        return "Воет на луну";
    }
}
