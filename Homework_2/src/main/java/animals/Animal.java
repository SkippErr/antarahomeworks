package animals;

import food.Food;

public abstract class Animal {

    protected String name;
    protected int satietly;

    public String getName() {
        return name;
    }

    public void getSatietly() {
        if (satietly <= 0) {
            System.out.println(getName() + " истощен(а)");
            return;
        }
        System.out.println("Сытость " + getName() + ": " + satietly);
    }

    public abstract boolean eat(Food food);
}
