package animals;

import actions.Voice;
import actions.Run;

public class Bear extends Carnivorous implements Run, Voice {

    public Bear(String name) {
        this.name = "Медведь " + name;
        satietly = 0;
    }

    public void run() {
        System.out.println(getName() + " ходит вперевалочку");
        satietly--;
    }

    public String voice() {
        return "рычит";
    }
}
