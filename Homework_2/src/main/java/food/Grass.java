package food;

public class Grass extends Food {

    public Grass(String name, int saturation) {
        this.name = name;
        this.saturation = saturation;
    }

    public String getName() {
        return name;
    }

    public int getSaturation() {
        return saturation;
    }
}
